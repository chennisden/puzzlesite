use chrono::NaiveDate;
use serde::Deserialize;
use std::fmt::Write;
use std::fs;
use std::path::{Path, PathBuf};

#[derive(Clone, Deserialize)]
struct Links {
    ctc: Option<String>,
    penpa: Option<String>,
    fpuzzles: Option<String>,
    octopuzzles: Option<String>,
    sudokulab: Option<String>,
    lmd: Option<String>,
}

#[derive(Deserialize)]
struct SerializedPuzzle {
    title: String,
    difficulty: u8,
    tags: Option<Vec<String>>,
    links: Links,
    desc: Option<String>,
    date: NaiveDate,
    example: Option<String>,
    hints: Option<Vec<String>>,
}

#[derive(Clone)]
struct Puzzle {
    title: String,
    path: String,
    // Most would opt for 1-5. Only a lunatic would overflow a u8
    difficulty: u8,
    // probably shouldn't make your tags empty, but we'll support it with Option<Vec<String>> when
    // serializing to SerializedPuzzle
    tags: Vec<String>,
    links: Links,
    desc: Option<String>,
    date: NaiveDate,
    // Just one link for an example puzzle; honestly there's no point having multiple
    example: Option<String>,
    hints: Option<Vec<String>>,
}

impl Puzzle {
    fn generate_link_html(&self) -> Vec<(PathBuf, String)> {
        let mut links: Vec<(PathBuf, String)> = Vec::new();
        if let Some(s) = &self.links.ctc {
            links.push((
                Path::new("build").join(&self.path).join("ctc.html"),
                format!("<meta http-equiv=\"Refresh\" content=\"0; url='{}'\">", s),
            ));
        }
        if let Some(s) = &self.links.penpa {
            links.push((
                Path::new("build").join(&self.path).join("penpa.html"),
                format!("<meta http-equiv=\"Refresh\" content=\"0; url='{}'\">", s),
            ));
        }
        if let Some(s) = &self.links.fpuzzles {
            links.push((
                Path::new("build").join(&self.path).join("f-puzzles.html"),
                format!("<meta http-equiv=\"Refresh\" content=\"0; url='{}'\">", s),
            ));
        }
        if let Some(s) = &self.links.sudokulab {
            links.push((
                Path::new("build").join(&self.path).join("sudokulab.html"),
                format!("<meta http-equiv=\"Refresh\" content=\"0; url='{}'\">", s),
            ));
        }
        if let Some(s) = &self.example {
            links.push((
                Path::new("build").join(&self.path).join("example.html"),
                format!("<meta http-equiv=\"Refresh\" content=\"0; url='{}'\">", s),
            ));
        }
        links
    }

    fn to_html(&self, img_ext: &str) -> String {
        let mut html: String = String::new();

        write!(
            html,
            "
            <h2 id = \"{}\">{} <a href=\"#{}\">#</a></h2>
            <p>Published on {}.</p>
            <p><em>Difficulty: <a href=\"/?difficulty={}\">{}</a></em></p>
            ",
            self.path,
            self.title,
            self.path,
            self.date.format("%B %-d, %Y"),
            self.difficulty,
            self.difficulty,
        )
        .unwrap();

        if !self.tags.is_empty() {
            write!(
                html,
                "<p><em>Tags: {}</em></p>",
                self.tags
                    .iter()
                    .map(|s| format!("<a href=\"/?tag={}\">{}</a>", s, s))
                    .collect::<Vec<_>>()
                    .join(", ")
            )
            .unwrap();
        }

        if let Some(desc) = &self.desc {
            for line in desc.lines() {
                write!(html, "<p>{}</p>", line).unwrap();
            }
        }

        write!(
            html,
            "<img src=\"{}.{}\" alt=\"Image of '{}' puzzle\">",
            self.path, img_ext, self.title
        )
        .unwrap();

        if self.links.ctc.is_some() {
            write!(html, "<p><a href=\"{}/ctc\">CTC</a></p>", self.path).unwrap();
        }
        if self.links.penpa.is_some() {
            write!(html, "<p><a href=\"{}/penpa\">Penpa</a></p>", self.path).unwrap();
        }
        if self.links.fpuzzles.is_some() {
            write!(
                html,
                "<p><a href=\"{}/f-puzzles\">F-Puzzles</a></p>",
                self.path
            )
            .unwrap();
        }
        if let Some(link) = &self.links.octopuzzles {
            write!(html, "<p><a href=\"{}\">OctoPuzzles</a></p>", link).unwrap();
        }
        if self.links.sudokulab.is_some() {
            write!(
                html,
                "<p><a href=\"{}/sudokulab\">SudokuLab</a></p>",
                self.path
            )
            .unwrap();
        }
        if let Some(link) = &self.links.lmd {
            write!(html, "<p><a href=\"{}\">LMD</a></p>", link).unwrap();
        }

        if self.example.is_some() {
            write!(
                html,
                "<p>An example puzzle is shown below.</p><img src=\"{}-example.{}\" alt=\"Image of '{}' example puzzle\"><br><br><img src=\"{}-example-sol.{}\" alt=\"Image of '{}' example puzzle solution\"><p><a href=\"{}/example\">Example</a></p>",
                self.path,
                img_ext,
                self.title,
                self.path,
                img_ext,
                self.title,
                self.path
            ).unwrap();
        }

        if let Some(hints) = &self.hints {
            for (idx, hint) in hints.iter().enumerate() {
                write!(
                    html,
                    "<details><summary><strong>Hint {}</strong></summary><div>{}</div></details>",
                    idx + 1,
                    hint
                )
                .unwrap();
            }
        }

        html
    }
}

/// Struct used when we look up puzzles and return their pre-built HTML

#[derive(Clone)]
pub struct PuzzleHTML {
    puzzle: Puzzle,
    pub html: String,
}

impl PuzzleHTML {
    pub fn check(&self, difficulty: Option<u8>, tag: &Option<String>) -> bool {
        if let Some(difficulty) = difficulty {
            if self.puzzle.difficulty != difficulty {
                return false;
            }
        }
        if let Some(tag) = tag {
            if !self.puzzle.tags.contains(tag) {
                return false;
            }
        }
        true
    }
}

// returns extension

fn cp_puzz_img(dir: &Path, puzzpath: &str) -> Option<String> {
    // Copy img, get ext, use ext to gen HTML
    // Order of img_exts matters because it's the order the program will look for files in
    let img_exts = ["svg", "png", "jpg", "jpeg"];

    for ext in img_exts {
        let from_path = dir.join(format!("puzzle.{}", ext));
        let to_path = Path::new("build").join(format!("{}.{}", puzzpath, ext));
        if from_path.exists() {
            fs::copy(from_path, to_path)
                .unwrap_or_else(|_| panic!("Failed to copy puzzle.{}", ext));
            return Some(ext.into());
        }
    }

    None
}

// Rarely used, just for example puzzles

fn cp_puzz_img_known_ext(dir: &Path, puzzpath: &str, name: &str, ext: &str) -> std::io::Result<()> {
    let from_path = dir.join(format!("{}.{}", name, ext));
    let to_path = Path::new("build").join(format!("{}-{}.{}", puzzpath, name, ext));
    fs::copy(from_path, to_path)?;
    Ok(())
}

/// Builds the static portion of the puzzle website (i.e. index.html + copied images + puzzle
/// images) in a build/ directory
///
/// Returns:
/// - template string
/// - vector of puzzles and generated HTML; we will read it in main.rs, keep that value, and use it
///   for any query strings

pub fn build() -> (String, Vec<PuzzleHTML>) {
    // Make sure that build/ is directory

    if let Ok(metadata) = fs::metadata("build") {
        if !metadata.file_type().is_dir() {
            panic!("File build/ exists but is not a directory")
        }
    } else if fs::create_dir("build").is_err() {
        panic!("Could not create directory build/false");
    }

    // Copy css and images files

    let extensions = ["css", "svg", "png", "jpg", "jpeg"];

    for file in fs::read_dir(".").unwrap() {
        let path = file.as_ref().unwrap().path();
        if let Some(ext) = path.extension().and_then(std::ffi::OsStr::to_str) {
            if extensions.contains(&ext) {
                fs::copy(&path, Path::new("build").join(&path)).unwrap();
            }
        }
    }

    // Reads every puzzle, generates their HTML, and copies puzzle images
    // We're just gonna assume everything in the puzzles/ directory is a directory of an individual
    // puzzle

    let mut puzzles: Vec<PuzzleHTML> = Vec::new();

    for path in fs::read_dir("puzzles").unwrap() {
        if let Ok(puzzle) = path {
            let dir = puzzle.path();
            let puzzle_build_dir = Path::new("build").join(&dir);
            if !puzzle_build_dir.exists() {
                fs::create_dir_all(&puzzle_build_dir).unwrap();
            }

            // Read puzzle
            let serializedpuzzle: SerializedPuzzle = toml::from_str(
                &fs::read_to_string(dir.join("config.toml"))
                    .unwrap_or_else(|_| panic!("Could not read config.toml in puzzle {:?}", dir)),
            )
            .unwrap_or_else(|_| panic!("Could not serialize config.toml in puzzle {:?}", dir));
            let puzzle = Puzzle {
                title: serializedpuzzle.title,
                difficulty: serializedpuzzle.difficulty,
                tags: match serializedpuzzle.tags {
                    Some(tags) => tags,
                    None => Vec::new(),
                },
                links: serializedpuzzle.links,
                desc: serializedpuzzle.desc,
                date: serializedpuzzle.date,
                path: dir
                    .file_name()
                    .unwrap_or_else(|| panic!("{:?} is not valid UTF-8", dir))
                    .to_str()
                    .unwrap()
                    .to_string(),
                example: serializedpuzzle.example,
                hints: serializedpuzzle.hints,
            };

            // Generate HTML for links
            let puzzle_dir = Path::new("build").join(&puzzle.path);
            if !puzzle_dir.exists() {
                fs::create_dir(puzzle_dir).unwrap();
            }
            let links = puzzle.generate_link_html();
            for link in links {
                fs::write(link.0, link.1).unwrap();
            }

            // Copy image puzzle, use extension to gen HTML
            match cp_puzz_img(&dir, &puzzle.path) {
                Some(ext) => {
                    let html = puzzle.to_html(&ext);
                    // Copy example puzzle and solution img if it exists
                    if puzzle.example.is_some() {
                        if cp_puzz_img_known_ext(&dir, &puzzle.path, "example", &ext).is_err() {
                            println!("Couldn't copy example.{} for puzzle {}", ext, puzzle.path);
                        }
                        if cp_puzz_img_known_ext(&dir, &puzzle.path, "example-sol", &ext).is_err() {
                            println!(
                                "Couldn't copy example-sol.{} for puzzle {}",
                                ext, puzzle.path
                            );
                        }
                    }
                    puzzles.push(PuzzleHTML { puzzle, html });
                }
                None => {
                    println!("Missing image for puzzle {}", puzzle.path);
                }
            }
        } else {
            println!(
                "Bug: could not unwrap DirEntry into Path in puzzle {:?}",
                path
            );
        }
    }

    puzzles.sort_by(|a, b| b.puzzle.date.partial_cmp(&a.puzzle.date).unwrap());

    let mut index_html = String::new();

    for puzzle in &puzzles {
        index_html += &puzzle.html;
    }

    let template = fs::read_to_string("template.html").unwrap();
    fs::write("build/index.html", template.replace("$body$", &index_html)).unwrap();

    (template, puzzles)
}
