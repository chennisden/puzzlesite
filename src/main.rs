mod puzzle;
use crate::puzzle::PuzzleHTML;
use axum::{
    extract::Query,
    response::{Html, IntoResponse, Response},
    routing::get,
    Extension, Router,
};
use serde::Deserialize;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Sync + Send>> {
    let (template, puzzles) = puzzle::build();
    // Though the app will only server-side generate index.html, you should use NGINX and only redirect
    // stuff to the app if it has a query param.
    let app = Router::new()
        .route("/", get(index))
        .layer(Extension(template))
        .layer(Extension(puzzles));
    let addr = SocketAddr::new(
        IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)),
        match std::env::args().nth(1) {
            Some(port) => port.parse().expect("Argument is not a port"),
            None => 80,
        },
    );
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await?;
    Ok(())
}

#[derive(Deserialize)]
pub struct PuzzQuery {
    difficulty: Option<u8>,
    tag: Option<String>,
}

// index.html

async fn index(
    Query(query): Query<PuzzQuery>,
    Extension(template): Extension<String>,
    Extension(puzzles): Extension<Vec<PuzzleHTML>>,
) -> Response {
    let mut html = String::new();
    for puzzle in puzzles {
        if puzzle.check(query.difficulty, &query.tag) {
            html += &puzzle.html;
        }
    }
    Html(template.replace("$body$", &html)).into_response()
}
